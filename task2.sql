drop table t_barriers;
drop table t_users;

create table t_barriers
(
    id int primary key,
    id_user int,
    in_out numeric(1),
    f_date timestamp
);
create table t_users
(
    id_user int primary key,
    name_user varchar(500)
);
insert into t_users values (1, 'ivan');
insert into t_users values (2, 'sergey');
insert into t_users values (3, 'petr');
insert into t_users values (4, 'alexey');

insert into t_barriers values (1, 1, 0, '01-01-2001 10:00');
insert into t_barriers values (2, 1, 1, '01-01-2001 11:00');
insert into t_barriers values (3, 1, 0, '01-01-2001 12:00');
insert into t_barriers values (4, 4, 0, '01-01-2001 10:01');
insert into t_barriers values (5, 1, 1, '01-01-2001 13:00');
insert into t_barriers values (6, 4, 1, '01-01-2001 11:00');
insert into t_barriers values (7, 4, 0, '01-01-2001 23:00'); -- sleeps in the office

insert into t_barriers values (8, 2, 0, '02-01-2001 10:01');
insert into t_barriers values (9, 2, 1, '02-01-2001 11:00');
insert into t_barriers values (10, 2, 1, '02-01-2001 11:00');

    
-- all users, who were out more than 2 times
select u.name_user from t_users u
join t_barriers b on u.id_user = b.id_user
where b.in_out = 1 and date(b.f_date) = date('01-01-2001')
group by u.id_user having count(b.in_out) > 1; -- made 1, but it is 2 in the task

-- workday in hours
select u.name_user, extract(HOUR from max(b.f_date::time) - min(b.f_date::time)) work_hours from t_users u
join t_barriers b on u.id_user = b.id_user
where date(b.f_date) = date('01-01-2001')
group by u.id_user;

-- last out minus first in
with ins as (select id_user, f_date::time t from t_barriers where date(f_date) = date('01-01-2001') and in_out = 0),
outs as (select id_user, f_date::time t from t_barriers where date(f_date) = date('01-01-2001') and in_out = 1)
select u.name_user, extract(HOUR from max(outs.t) - min(ins.t)) out_minus_in from t_users u
join ins on u.id_user = ins.id_user
join outs on u.id_user = outs.id_user
group by u.id_user;




    
