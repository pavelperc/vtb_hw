package vtb_hw;

import java.sql.*;
import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {
        try (Connection conn = DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/vtb", "postgres", "12345678")) {

            prepareData(conn);
            var dates = getServerTime(conn);

            System.out.println("Got dates:");
            System.out.println(dates);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static void prepareData(Connection conn) throws SQLException {
        try {
            conn.prepareStatement("drop table server_time").execute();
        } catch (SQLException e) {
            // table does not exists
            System.out.println("Old table was not deleted: " + e.getMessage());
        }
        var st = conn.createStatement();
        st.addBatch("create table server_time(sysdate date, dummy int)");
        st.addBatch("insert into server_time values ('01.01.2000', 1)");
        st.addBatch("insert into server_time values ('02.01.2000', 0)");
        st.addBatch("insert into server_time values ('03.01.2000', 1)");
        st.addBatch("insert into server_time values ('04.01.2000', 0)");
        st.executeBatch();
    }

    private static ArrayList<Object> getServerTime(Connection conn) throws SQLException {
        PreparedStatement st = conn.prepareStatement("select sysdate from server_time where dummy = ?");
        st.setInt(1, 0);
        ResultSet rs = st.executeQuery();
        var dates = new ArrayList<>();

        while (rs.next()) {
            dates.add(rs.getDate(1));
        }
        return dates; // returns [2000-01-02, 2000-01-04]
    }
}

